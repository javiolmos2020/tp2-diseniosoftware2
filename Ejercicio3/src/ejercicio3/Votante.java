
package ejercicio3;


public class Votante {
    private int edad;

    public Votante(int edad) {
        this.edad = edad;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
       
    
public void tipoDeVotante(){
    final int edadMinima = 18;
    final int edadMaxima = 130;
    
    if (edad<=edadMaxima && edad>=edadMinima){
        System.out.println("Votante habilitado");
       
    } else 
        System.out.println("Votante inhabilitado");
}
    
  
    
    
}

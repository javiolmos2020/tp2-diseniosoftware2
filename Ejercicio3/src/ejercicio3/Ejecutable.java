/*Ejercicio 3
Codifique un programa que defina dos valores constantes que representan la edad 
mínima y la edad máxima para poder votar. Luego verifique que dada una persona 
se indique si la edad de la misma lo habilita para votar. Muestre por pantalla si el 
mensaje “Votante habilitado” o “Votante inhabilitado en cada caso*/

package ejercicio3;


public class Ejecutable {

   
    public static void main(String[] args) {
        Votante votante = new Votante(25);
        Votante votante2 = new Votante(14);
        Votante votante3 = new Votante(75);
        
        votante.tipoDeVotante();
        votante2.tipoDeVotante();
        votante3.tipoDeVotante();

    }
    
}

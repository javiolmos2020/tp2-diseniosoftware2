package ejercicio6;

public class Ejecutable {

   
    public static void main(String[] args) {
        Producto producto1 = new Producto(0001, "Shampoo", 300.50, 0.50);
        Producto producto2 = new Producto(0002, "Crema enjuague", 400.45, 0.38);
        
        producto1.mostrarDatosProducto();
        producto2.mostrarDatosProducto();
        
        producto1.compararPrecioVenta(producto2);
    }
}
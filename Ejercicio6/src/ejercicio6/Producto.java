
package ejercicio6;


public class Producto {
    private int codigo;
    private String nombre;
    private double precioDeCosto;
    private double porcentajeGanancia;
    private double iva;
    private double precioVenta;
    
    public Producto(int codigo, String nombre, double precioCosto, double porcentajeGanancia) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precioDeCosto = precioCosto;
        this.porcentajeGanancia = porcentajeGanancia;
        this.iva = 0.21;
        this.precioVenta = calcularPrecioDeVenta();
    }
    
    // Métodos accesores para cada atributo
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public int getCodigo() {
        return codigo;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setPrecioCosto(double precioCosto) {
        this.precioDeCosto = precioCosto;
    }
    
    public double getPrecioCosto() {
        return precioDeCosto;
    }
    
    public void setPorcentajeGanancia(double porcentajeGanancia) {
        this.porcentajeGanancia = porcentajeGanancia;
    }
    
    public double getPorcentajeGanancia() {
        return porcentajeGanancia;
    }
    
    public void setIva(double iva) {
        this.iva = iva;
    }
    
    public double getIva() {
        return iva;
    }
    
    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }
    
    public double getPrecioVenta() {
        return precioVenta;
    }
    
      public void mostrarDatosProducto() {
        
        System.out.println("---------------Información del producto---------------");  
        System.out.println("Código: " + codigo);
        System.out.println("Nombre: " + nombre);
        System.out.println("Precio de costo: " + precioDeCosto);
        System.out.println("Porcentaje de ganancia: " + porcentajeGanancia);
        System.out.println("IVA: " + iva);
        System.out.println("Precio de venta: " + precioVenta); 
    }
      
    private double calcularPrecioDeVenta() {
        double ganancia = precioDeCosto * porcentajeGanancia;
        double precioSinIva = precioDeCosto + ganancia;
        double precioConIva = precioSinIva + precioSinIva * iva;
        return precioConIva;
    }
    
     public void compararPrecioVenta(Producto producto2) {
        if (precioVenta > producto2.getPrecioVenta()) {
            System.out.println("El producto que tiene mayor precio es: " + nombre);
        } else  if (precioVenta < producto2.getPrecioVenta()) {
            System.out.println("El producto que tiene mayor precio es: " + producto2.getNombre());
        }
    
    }

    
    
}

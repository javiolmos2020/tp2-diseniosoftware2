/*Ejercicio 4
Escribir un programa en el cual se debe definir un array de enteros de 10 posiciones, inicializarlo con números enteros, luego definir métodos para que obtenga la 
suma y la cantidad de los elementos pares, la suma y la cantidad de los elementos 
impares, el valor máximo de los elementos pares y el valor mínimo de los elementos impares. Mostrar los resultados obtenidos por pantalla.
*/
package ejercicio4;

public class Ejecutable {

    
    public static void main(String[] args) {
    
        ArrayEnteros arrayDeNumeros = new ArrayEnteros (new int [] {5,10,15,20,25,30,35,40,45,50});
        /*        Array[] arrayDeNumeros = new Array [10];
            espacio para pensar otra forma de crear el objeto array */
        
        for (int i=0;i<arrayDeNumeros.getArray().length;i++){
            System.out.println("El numero en la posicion " + i + " es: " + arrayDeNumeros.getArray()[i]);
        }
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("Para el primer array de numeros enteros tenemos la siguiente información:");
        System.out.println("---------------------------------------------------------------------------");
        arrayDeNumeros.sumaPares();
        System.out.println("La cantidad de elementos pares es: " + arrayDeNumeros.cantidadNumerosPares());
        arrayDeNumeros.sumaImpares();
        System.out.println("La cantidad de elementos impares es: " + arrayDeNumeros.cantidadNumerosImares());
        arrayDeNumeros.valorMaximoDePares();
        arrayDeNumeros.valorMinimoDeImpares();
    }
    
}

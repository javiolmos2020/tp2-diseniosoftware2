
package ejercicio4;

import java.util.Arrays;


public class ArrayEnteros {
    private int [] array;

    public ArrayEnteros(int[] array) {
        this.array = array;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public void sumaPares(){
    int sumaPares= Arrays.stream(array).filter(i->i%2 ==0).sum();
        System.out.println("La suma de los elementos pares es: " + sumaPares);        
            }
    
    public int cantidadNumerosPares(){
        int cantidad=0;
        for(int i=0; i<array.length;i++){
           if(array[i]%2==0){
            cantidad++;
           }
        }
        return cantidad;
        
    }
 
    public void sumaImpares(){
       int sumaImpares= Arrays.stream(array).filter(i->i%2 ==1).sum();
        System.out.println("La suma de los elementos impares es: " + sumaImpares);    
    }
    
    public int cantidadNumerosImares(){
        int cantidad=0;
        for(int i=0; i<array.length;i++){
           if(array[i]%2==0){
            cantidad++;
           }
        }
        return cantidad;
        
    }
   
    public void valorMaximoDePares(){
        int valorMaximo= Arrays.stream(array).max().getAsInt();
        System.out.println("El numero maximo del array es: " + valorMaximo);
       
    }
    
    public void valorMinimoDeImpares(){
        int valorMinimo= Arrays.stream(array).min().getAsInt();
        System.out.println("El numero minimo del array es: " + valorMinimo);
    }
    
}

/*
 Ejercicio 2
Elaborar un programa maneje tres valores que representan los lados de un triángulo e imprima el tipo de triángulo al cual corresponde, teniendo en cuenta que de 
acuerdo a la igualdad o desigualdad de sus lados, los triángulos se clasifican en:
Escaleno: todos sus lados son diferentes.
Isósceles: al menos dos de sus lados son iguales.
Equilátero: los tres lados son iguales.
 */
package ejercicio2;


public class Ejecutable {

    public static void main(String[] args) {
       Triangulo triangulo = new Triangulo(12,12,12);
       Triangulo triangulo2 = new Triangulo(15,9,9);
       Triangulo triangulo3 = new Triangulo(10,11,12);   
       
       
       triangulo.tipoDeTriangulo();
       triangulo2.tipoDeTriangulo();
       triangulo3.tipoDeTriangulo();
    }
    
}

package ejercicio5;


public class Pelicula {

    private String nombrePelicula;
    private String genero;
    private int duracion;
    private String calificacion;
    private String director;

    public Pelicula(String nombrePelicula, String genero, int duracion, String calificacion, String director) {
        this.nombrePelicula = nombrePelicula;
        this.genero = genero;
        this.duracion = duracion;
        this.calificacion = calificacion;
        this.director = director;
    }

    public String getNombrePelicula() {
        return nombrePelicula;
    }

    public void setNombrePelicula(String nombrePelicula) {
        this.nombrePelicula = nombrePelicula;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getDuración() {
        return duracion;
    }

    public void setDuración(int duración) {
        this.duracion = duración;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }
    
    public void mostrarDatosPelicula(){
        System.out.println("------------------------------------------------------------");
        System.out.println("El nombre de la película es: " + nombrePelicula);
        System.out.println("El genero de la película es: " + genero);
        System.out.println("El nombre de la película es: " + duracion);
        System.out.println("La calificación la película es: " + calificacion);
        System.out.println("Director/es: " + director);
        System.out.println("------------------------------------------------------------");
    }
}

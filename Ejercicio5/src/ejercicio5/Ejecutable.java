/*Ejercicio 5
Codifique una clase que tenga atributos que representen el título de una película, 
su género, duración, calificación de acuerdo a la edad (ATP (Aptas para Todo Público), PM13 (mayores de trece años), PM16 (mayores de dieciséis) y PM18 (sólo 
para adultos), y director todos los atributos deben ser privados. Defina los métodos 
necesarios para establecer y obtener los valores de los atributos y otro que permita 
mostrar sus valores. Además, defina otra clase que permita instanciar 2 objetos de 
la clase Película y muestre sus datos.*/


package ejercicio5;


public class Ejecutable {

    
    public static void main(String[] args) {
        Pelicula peli= new Pelicula ("Star Wars III: La venganza de los sith","Ciencia ficción",140,"PM13","George Lucas");
        Pelicula peli2= new Pelicula ("Shrek 2","Aventura",90,"ATP","Andrew Adamson - Kelly Asbury - Conrad Vernom");
     
        peli.mostrarDatosPelicula();
        peli2.mostrarDatosPelicula();
    }
    
}

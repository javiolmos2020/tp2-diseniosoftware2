package ejercicio1;

public class Cadena {
    private String cadena;

    public Cadena(String cadena) {
        this.cadena = cadena;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    
    public void dameSubCadena(){
       int tamanioCadena = cadena.length();
       String subCadena= cadena.substring(0,tamanioCadena/2);
       System.out.println("Esta es la subcadena: " + subCadena );
        
    }

    public void mostrarEnMayusculas(){
        String mayusculas = cadena.toUpperCase();
        System.out.println("La cadena en mayúsculas es: " + mayusculas);
    }
    
    public void mostrarUltimoCaracter(){
        char ultimoCaracter = cadena.charAt(cadena.length()-1);
        System.out.println("El ultimo caracter es: " + ultimoCaracter);          
    }
    
    public void invertirCadena(){
        StringBuilder auxiliar = new StringBuilder(cadena);
        auxiliar.reverse();
        String cadenaInvertida = auxiliar.toString();
        System.out.println("La cadena invertida es: " + cadenaInvertida);
    }
    public void mostrarCadenaConGuiones(){
        String[] cadenaSeparada = cadena.split("");
        System.out.println("La cadena separada por guiones es: " + String.join("-", cadenaSeparada));
    }


    public void mostrarCantidadVocales(){
    int cantidadVocales = 0;
    for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.charAt(i);
            if (caracter == 'a'||caracter == 'e'||caracter == 'i'||caracter == 'o'||caracter == 'u') {
                cantidadVocales++;
            }
        }
        System.out.println("La cadena tiene " + cantidadVocales + " vocales.");
    }

}


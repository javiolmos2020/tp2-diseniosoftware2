/*Ejercicio 1 
Codifique una clase que dado una cadena de caracteres en minúscula permita 
la siguiente funcionalidad:
i. Mostrar la primera mitad de los caracteres de la cadena.
ii. Mostrarla en mayúscula
iii. Mostrar el último carácter.
iv. Mostrarla en forma inversa.
v. Mostrar cada caracter de la cadena separado con un guion.
vi. Mostrar la cantidad de vocales contenidas en la cadena de caracteres.
*/

package ejercicio1;


public class Ejecutable {

   
    public static void main(String[] args) {
        
       Cadena cadenaDeCaracteres = new Cadena("Javier");
       Cadena cadenaDeCaracteres2 = new Cadena ("Estudiante");
       Cadena cadenaDeCaracteres3 = new Cadena ("Ejercicio");  
      
      cadenaDeCaracteres.dameSubCadena();
      cadenaDeCaracteres2.dameSubCadena();
      cadenaDeCaracteres3.dameSubCadena(); 
      
      cadenaDeCaracteres.mostrarEnMayusculas();
      cadenaDeCaracteres2.mostrarEnMayusculas();
      cadenaDeCaracteres3.mostrarEnMayusculas(); 
     
      cadenaDeCaracteres.mostrarUltimoCaracter();
      cadenaDeCaracteres2.mostrarUltimoCaracter();
      cadenaDeCaracteres3.mostrarUltimoCaracter(); 
    
      cadenaDeCaracteres.invertirCadena();
      cadenaDeCaracteres2.invertirCadena();
      cadenaDeCaracteres3.invertirCadena();
    
      cadenaDeCaracteres.mostrarCadenaConGuiones();
      cadenaDeCaracteres2.mostrarCadenaConGuiones();
      cadenaDeCaracteres3.mostrarCadenaConGuiones();
      
      cadenaDeCaracteres.mostrarCantidadVocales();
      cadenaDeCaracteres2.mostrarCantidadVocales();
      cadenaDeCaracteres3.mostrarCantidadVocales();
    }
    
}
